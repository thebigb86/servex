<?php

namespace Servex\Core;

class DefaultConfiguration extends Configuration
{
	public function __construct()
	{
		$this->setAppNamespace('App');
		$this->setAppSourceDir('app');
	}
} 