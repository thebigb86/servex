<?php

namespace Servex\Core;

class Configuration
{
    /**
     * @var string Namespace in which the user application resides.
     */
    private $appNamespace;

	/**
	 * @var string Directory relative to the server's document root in which the user application files reside.
	 */
	private $appSourceDir;

	/**
	 * @return string
	 */
	public function getAppNamespace()
	{
		return $this->appNamespace;
	}

	/**
	 * @param string $appNamespace
	 */
	public function setAppNamespace($appNamespace)
	{
		$this->appNamespace = $appNamespace;
	}

	/**
	 * @return string
	 */
	public function getAppSourceDir()
	{
		return $this->appSourceDir;
	}

	/**
	 * @param string $appSourceDir
	 */
	public function setAppSourceDir($appSourceDir)
	{
		$this->appSourceDir = $appSourceDir;
	}
}