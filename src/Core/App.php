<?php

namespace Servex\Core;

use ArrayAccess;
use Silex\Application;

class App implements ArrayAccess
{
	#region Fields

	/**
	 * @var Configuration Holds the configuration for the service instance
	 */
	private $configuration;

	/**
	 * @var Application Silex instance
	 */
	private $silex;

	#endregion

	#region Methods

	/**
	 * App initializer. If the $configuration parameter is omitted or provided a null-value, an instance of
	 * \Servex\Core\DefaultConfiguration will be used.
	 *
	 * @param null|Configuration $configuration
	 */
	public function __construct($configuration = null)
	{
		if($configuration === null)
			$configuration = new DefaultConfiguration();

		$this->configuration = $configuration;

		$this->silex = new Application();
		$this->loadResources();
	}

	private function loadResources()
	{

	}

	/**
	 * Start processing the request
	 */
	public function run()
	{
		$this->silex->run();
	}

	#endregion

	#region Getters and setters

	/**
	 * @return Configuration
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}

	/**
	 * @param Configuration $configuration
	 */
	public function setConfiguration($configuration)
	{
		$this->configuration = $configuration;
	}

	/**
	 * @return Application
	 */
	public function getSilex()
	{
		return $this->silex;
	}

	/**
	 * @param Application $silex
	 */
	public function setSilex($silex)
	{
		$this->silex = $silex;
	}

	#endregion

	#region Pass-through to Silex DI container

	/**
	 * Pass-through to Silex DI container
	 *
	 * @param mixed $offset
	 * @return bool
	 */
	public function offsetExists($offset)
	{
		return isset($this->silex[$offset]);
	}

	/**
	 * Pass-through to Silex DI container
	 *
	 * @param mixed $offset
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		return $this->silex[$offset];
	}

	/**
	 * Pass-through to Silex DI container
	 *
	 * @param mixed $offset
	 * @param mixed $value
	 */
	public function offsetSet($offset, $value)
	{
		$this->silex[$offset] = $value;
	}

	/**
	 * Pass-through to Silex DI container
	 *
	 * @param mixed $offset
	 */
	public function offsetUnset($offset)
	{
		unset($this->silex[$offset]);
	}

	#endregion
}